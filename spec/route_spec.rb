require 'spec_helper'
require 'route'

RSpec.describe Route do
  let(:city1) {
    "A"
  }
  let(:city2) {
    "D"
  }
  let(:distance){
    2
  }

  it('stores from_city, to_city and distance') do
    route = Route.new(city1,city2,distance)

    expect(route.from_city).to eq city1
    expect(route.to_city).to eq city2
    expect(route.distance).to eq distance
  end

end