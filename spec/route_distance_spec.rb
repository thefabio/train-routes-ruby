require 'spec_helper'
require 'route_distance'

RSpec.describe RouteDistance do
  let(:route) {
    "ABCD"
  }
  let(:distance){
    2
  }

  it('stores route path and distance') do
    route_distance = RouteDistance.new(route,distance)

    expect(route_distance.route).to eq route
    expect(route_distance.distance).to eq distance
  end

end