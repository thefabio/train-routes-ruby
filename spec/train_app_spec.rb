require 'spec_helper'
require 'train_app'
require 'graph'

RSpec.describe TrainApp do
  include_examples "shared data"
  train_app = nil
  before :each do
    train_app = TrainApp.new(graph_input)
  end

  it ('Outputs 9 for example #1') {
    expect(train_app.calculate_route_distance('ABC')).to eq 9
  }
  it ('Outputs 5 for example #2') {
    expect(train_app.calculate_route_distance('AD')).to eq 5
  }
  it ('Outputs 13 for example #3') {
    expect(train_app.calculate_route_distance('ADC')).to eq 13
  }
  it ('Outputs 22 for example #4') {
    expect(train_app.calculate_route_distance('AEBCD')).to eq 22
  }
  it ("Outputs '#{Graph::ROUTE_NOT_FOUNT_TEXT}' for example #5") {
    expect(train_app.calculate_route_distance('AED')).to eq Graph::ROUTE_NOT_FOUNT_TEXT
  }
  it ('Outputs 2 for example #6') {
    expect(train_app.number_of_trips_max_stops('C','C', 3)).to eq 2
  }
  it ('Outputs 3 for example #7') {
    expect(train_app.number_of_trips_exact_stops('A','C', 4)).to eq 3
  }
  it ('Outputs 9 for example #8') {
    expect(train_app.shortest_distance('A','C')).to eq 9
  }
  it ('Outputs 9 for example #9') {
    expect(train_app.shortest_distance('B','B')).to eq 9
  }
  it ('Outputs 7 for example #10') {
    expect(train_app.number_of_routes_with_max_distance('C','C', 30)).to eq 7
  }
end

