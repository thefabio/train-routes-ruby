require 'spec_helper'
require 'route'
require 'graph'

RSpec.describe Graph do
  include_examples "shared data"

  let(:route1) {
    Route.new('A','B',2)
  }
  let(:route2) {
    Route.new('C','B',13)
  }
  let(:route3) {
    Route.new('C','B', 15)
  }
  let(:route4) {
    Route.new('B','C', 15)
  }

  it('stores from_city, to_city and distance') do
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)

    expect(graph.routes.length).to eq 2
    expect(graph.routes[0].from_city).to eq route1.from_city
    expect(graph.routes[0].to_city).to eq route1.to_city
    expect(graph.routes[0].distance).to eq route1.distance
    expect(graph.routes[1].from_city).to eq route2.from_city
    expect(graph.routes[1].to_city).to eq route2.to_city
    expect(graph.routes[1].distance).to eq route2.distance
  end

  it('updates the route distance when the same route is updated') do
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)
    graph.add_route(route3)

    expect(graph.routes.length).to eq 2
    expect(graph.routes[0].from_city).to eq route1.from_city
    expect(graph.routes[0].to_city).to eq route1.to_city
    expect(graph.routes[0].distance).to eq route1.distance
    expect(graph.routes[1].from_city).to eq route2.from_city
    expect(graph.routes[1].to_city).to eq route2.to_city
    expect(graph.routes[1].distance).to eq route3.distance
  end

  it('parses data from input format into routes') do
    graph = Graph.new(graph_input)

    expect(graph.routes.length).to eq 9
    expect(graph.routes[8].from_city).to eq 'A'
    expect(graph.routes[8].to_city).to eq 'E'
    expect(graph.routes[8].distance).to eq 7
  end

  it('retrieves correct distance per route') do
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)

    expect(graph.get_route_distance('A','B')).to eq 2
    expect(graph.get_route_distance('C','B')).to eq 13
  end

  it('retrieves invalide distance when the route is not found') do
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)

    expect(graph.get_route_distance('A','C')).to eq Graph::ROUTE_NOT_FOUNT
  end

  it ('calculates route distances correctly') {
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)
    graph.add_route(route4)
    expect(graph.calculate_route_distance('ABC')).to eq 17
    expect(graph.calculate_route_distance('ABCB')).to eq 30
  }
  it ("calculates route should return #{Graph::ROUTE_NOT_FOUNT_TEXT} when route is invalid") {
    graph = Graph.new('')
    graph.add_route(route1)
    graph.add_route(route2)
    graph.add_route(route4)
    expect(graph.calculate_route_distance('AC')).to eq Graph::ROUTE_NOT_FOUNT_TEXT
  }

  it('calculates all the possible route paths when given a number of stops') do
    graph = Graph.new(graph_input)

    expect(graph.generate_possible_routes(1).length).to eq 9
    expect(graph.generate_possible_routes(2).length).to eq 13
    expect(graph.generate_possible_routes(3).length).to eq 19
  end
end