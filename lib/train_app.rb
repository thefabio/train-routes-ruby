require 'json'
require_relative 'graph'
class TrainApp
  def initialize(graph_data)
    @graph = Graph.new(graph_data)
  end

  def calculate_route_distance(route_data)
    @graph.calculate_route_distance(route_data)
  end

  def number_of_trips_max_stops(from_city, to_city, max_stops)
    possible_routes = @graph.generate_possible_routes_and_distances(max_stops, from_city, to_city)
    possible_routes.length
  end

  def number_of_trips_exact_stops(from_city, to_city, stops)
    possible_routes = @graph.generate_possible_routes(stops, from_city, to_city)
    possible_routes.length
  end

  def shortest_distance(from_city, to_city)
    #TODO: although 6 is enough to pass the current use cases this function might miss paths in a more complex graph setup
    possible_routes = @graph.generate_possible_routes_and_distances(6,from_city, to_city)
    possible_routes.sort_by { |r| r.distance }.first.distance
  end

  def number_of_routes_with_max_distance(from_city, to_city, max_distance)
    #TODO: although 9 is enough to pass the current use case, it takes a little over 12 seconds complete so there is space for optimizations
    possible_routes = @graph.generate_possible_routes_and_distances(9, from_city, to_city)
    possible_routes_filtered = possible_routes.select { |r| r.distance <  max_distance}

    #puts possible_routes_filtered.map{|r| "#{r.route}:#{r.distance}"}.to_json
    possible_routes_filtered.length
  end
end
