class Route
  attr_reader :from_city
  attr_reader :to_city
  attr_reader :distance
  def initialize(from_city, to_city, distance)
    @from_city = from_city
    @to_city = to_city
    @distance = distance.to_i
  end
end