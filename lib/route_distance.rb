class RouteDistance
  attr_reader :route
  attr_reader :distance
  def initialize(route, distance)
    @route = route
    @distance = distance.to_i
  end
end
