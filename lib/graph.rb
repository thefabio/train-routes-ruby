require_relative 'route'
require_relative 'route_distance'
class Graph
  ROUTE_NOT_FOUNT = -999
  ROUTE_NOT_FOUNT_TEXT = 'NO SUCH ROUTE'
  attr_reader :routes

  def initialize(graph_data)
    @routes = []

    graph_data.split(',').each do |g|
      route_fields = g.strip.split('')
      add_route Route.new(route_fields[0], route_fields[1], route_fields[2])
    end
  end

  def add_route(route)
    existing_route = @routes.select { |r| r.from_city == route.from_city && r.to_city == route.to_city }

    if existing_route.count > 0
      @routes.delete(existing_route[0])
    end

    @routes << route
  end

  def get_route_distance(from_city, to_city)
    existing_route = @routes.select { |r| r.from_city == from_city && r.to_city == to_city }

    if existing_route.count > 0
      return existing_route[0].distance
    end

    ROUTE_NOT_FOUNT
  end

  def calculate_route_distance(route_data)
    route_data_items = route_data.split('')

    distance = 0
    for i in 0..(route_data_items.count - 2)
      new_distance = get_route_distance(route_data_items[i], route_data_items[i + 1])
      if new_distance == ROUTE_NOT_FOUNT
        return ROUTE_NOT_FOUNT_TEXT
      end
      distance += new_distance
    end
    distance
  end

  def generate_possible_routes(number_of_stops, from_city = nil, to_city = nil)
    all_cities = []
    @routes.each do |r|
      all_cities << r.from_city
      all_cities << r.to_city
    end
    all_cities.uniq!

    #TODO: this block is the cause of the delay, so optimise here when we have the time
    condition = !from_city.nil? && !to_city.nil?
    all_route_permutations = all_cities.
        repeated_permutation(number_of_stops + 1).select { |p|
      if condition
        p[0] == from_city && p[number_of_stops] == to_city
      else
        true
      end
    }.map(&:join)
    #end of the slow block

    existing_routes = []
    all_route_permutations.each do |p|
      distance = calculate_route_distance(p)
      unless distance == ROUTE_NOT_FOUNT_TEXT
        existing_routes << RouteDistance.new(p, distance)
      end
    end
    existing_routes
  end

  def generate_possible_routes_and_distances(max_stops, from_city = nil, to_city = nil)
    possible_routes = []
    for i in 1..max_stops
      # puts "Calculating #{i} stops..."
      new_possible_routes = generate_possible_routes(i, from_city, to_city)
      if new_possible_routes.count > 0
        possible_routes = possible_routes.concat(new_possible_routes)
      end
    end
    possible_routes
  end
end
